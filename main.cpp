﻿//
// ver 3.2
//

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <climits>
#include <vector>
#include <string>
#include <queue>
#include <deque>
#include <list>
#include <stack>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <algorithm>
#include <sstream>

#define rep(i, n) for (int i = 0; i < (n); i++)
#define itrep(i, a) for (auto i = (a).begin(); i != (a).end(); i++)
#define REP(i, a, n) for (int i = (a); i <= (n); i++)
#define all(a) (a).begin(), (a).end()
#define mp(a, b) make_pair((a), (b))

#define INF 12345678

#define MAX_HEIGHT 100
#define MAX_WIDTH 100
#define COLOR_TYPES 3
#define DIRECTION_TYPES 4

#define OBSTACLE 'X'
#define EMPTY '.'
#define LANTERN '+'
#define MIRROR_1 '/'
#define MIRROR_2 '\\'

#define NO_ITEM -1
#define NO_LANTERN -1

using namespace std;

int dx[DIRECTION_TYPES] = { 1, 0, -1, 0 };
int dy[DIRECTION_TYPES] = { 0, -1, 0, 1 };

const double timeLimit = 10;

long long beginCycle;
unsigned long long getCycle() {
	unsigned int low, high;
	__asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
	return ((unsigned long long int)low) | ((unsigned long long int)high << 32);
}

double getTime() {
	return (double)(getCycle() - beginCycle) / 2500000000;
}

class Point {
public:
	int x;
	int y;

	Point() {}
	Point(int x, int y) {
		this->x = x;
		this->y = y;
	}
};

class Item {
public:
	int x;
	int y;
	int type;
	Item(int x, int y, int type) {
		this->x = x;
		this->y = y;
		this->type = type;
	}

	bool isLantern() {
		return type >= 0 && type <= 2;
	}

	bool isObstacle() {
		return type == OBSTACLE;
	}

	bool isMirror() {
		return type == MIRROR_1 || type == MIRROR_2;
	}

	string format() {
		stringstream ss;

		ss << y << " " << x << " ";

		if (type == 0) ss << 1;
		else if (type == 1) ss << 2;
		else if (type == 2) ss << 4;
		else ss << (char)type;

		return ss.str();
	}
};

class Solution {
public:
	int score;
	vector<Item> items;

	Solution() {
		score = 0;
	}

	vector<string> format() {
		vector<string> ret;

		for (Item &item : items) {
			if (item.type == NO_ITEM) continue;
			ret.push_back(item.format());
		}

		return ret;
	}

	bool operator< (const Solution &a) const {
		return score < a.score;
	}
};


class Solver {
protected:
	int H, W;

	vector<string> board;
	int costLantern;
	int costMirror;
	int costObtacle;
	int leftMirrors;
	int leftObstacles;

	bool inBoard(int x, int y) {
		return x >= 0 && x < W && y >= 0 && y < H;
	}

	bool isTraversable(int x, int y) {
		if (!inBoard(x, y)) return false;
		char ch = board[y][x];
		return ch == EMPTY || ch == MIRROR_1 || ch == MIRROR_2;
	}

	bool isMirror(int x, int y) {
		return board[y][x] == MIRROR_1 || board[y][x] == MIRROR_2;
	}

	bool isItemSet(int x, int y) {
		return board[y][x] != EMPTY;
	}

	bool isCrystalSet(int x, int y) {
		return board[y][x] >= '1' && board[y][x] <= '6';
	}

	int ctoi(char ch) {
		return ch - '0';
	}

	bool isSingleColor(int color) {
		return color == 1 || color == 2 || color == 4;
	}

	int reflectDirection(int direction, char mirror) {
		if (mirror == MIRROR_1) {
			if (direction == 0) return 1;
			if (direction == 1) return 0;
			if (direction == 2) return 3;
			if (direction == 3) return 2;
		} else if (mirror == MIRROR_2) {
			if (direction == 0) return 3;
			if (direction == 1) return 2;
			if (direction == 2) return 1;
			if (direction == 3) return 0;
		} else {
			cerr << "error occurs on line " << __LINE__ << endl;
			throw invalid_argument("This is not mirror");
		}
	}

	static unsigned long randXor() {
		static unsigned long x = 123456789, y = 362436069, z = 521288629, w = 88675123;
		unsigned long t = (x ^ (x << 11));
		x = y; y = z; z = w;
		return (w = (w ^ (w >> 19)) ^ (t ^ (t >> 8)));
	}

public:
	Solver(vector<string> targetBoard, int costLantern, int costMirror, int costObstacle, int maxMirrors, int maxObstacles) {
		this->board = targetBoard;
		this->costLantern = costLantern;
		this->costMirror = costMirror;
		this->costObtacle = costObstacle;
		this->leftMirrors = maxMirrors;
		this->leftObstacles = maxObstacles;

		H = targetBoard.size();
		W = targetBoard[0].size();
	}

	virtual Solution solve() = 0;
};


// 
// すべての空きセルにランタンを置いてみて、一番スコアが良くなったのを採用するのを繰り返す Solver。
// ランタンの配置を試す際、誤ったクリスタルに光が当たるならば鏡の設置を検討する。
// ランタン配置後、Obstacle を置いてスコアを最適化できる場所がないか探して、あれば置く。
// 
class GreedySolver : public Solver {
private:
	int mirrorThreshold = 1;

	Point* NOWHERE = new Point(-1, -1);

	Solution solution;
	int lighted[MAX_HEIGHT][MAX_WIDTH][COLOR_TYPES];
	int lanternColor[MAX_HEIGHT][MAX_WIDTH];

	// 各セルから上下左右に光を辿って直進したときにどのセルに辿り着くかを格納
	// そのセルに光が当たっていない場合、NOWHERE
	Point* destination[MAX_HEIGHT][MAX_WIDTH][DIRECTION_TYPES];

	int tryLightCrystal(int x, int y, int color) {
		return lightCrystal(x, y, color, true);
	}

	int lightCrystal(int x, int y, int color) {
		return lightCrystal(x, y, color, false);
	}

	int lightCrystal(int x, int y, int color, bool test) {
		int prevScore = calcCrystalDummyScore(x, y);
		lighted[y][x][color]++;
		int newScore = calcCrystalDummyScore(x, y);
		if (test) lighted[y][x][color]--;
		return newScore - prevScore;
	}

	int unlightCrystal(int x, int y, int color, bool test) {
		int prevScore = calcCrystalScore(x, y);
		lighted[y][x][color]--;
		int newScore = calcCrystalScore(x, y);
		if (test) lighted[y][x][color]++;
		return newScore - prevScore;
	}

	int calcCrystalScore(int x, int y) {
		return calcCrystalScore(x, y, false);
	}

	int calcCrystalDummyScore(int x, int y) {
		return calcCrystalScore(x, y, true);
	}

	// dummy = true の場合、2 色のクリスタルに 1 色正解の光があたっている場合加点評価する
	int calcCrystalScore(int x, int y, bool dummy) {
		if (!isCrystalSet(x, y)) {
			cerr << "error occurs on line " << __LINE__ << endl;
			throw invalid_argument("There is not crystal");
		}

		int color = 0;
		int colorNum = 0;
		rep(c, COLOR_TYPES) {
			if (lighted[y][x][c] == 0) continue;
			color |= 1 << c;
			colorNum++;
		}

		int score;
		if (color == 0) score = 0;
		else if (color != ctoi(board[y][x])) {
			if (!dummy) score = -10;
			else score = calcDummyScore(x, y, color);
		}
		else score = colorNum == 2 ? 30 : 20;

		return score;
	}

	// 2 色以上のクリスタルについて、その片方の色の光があたってる場合は加点。
	// ただし、クリスタルの周囲 4 マスのうち 3 マス以上が塞がれている場合、
	// 正解の 2 色を当てることは不可能なため加点は行わない。
	int calcDummyScore(int x, int y, int color) {
		int crystalColor = ctoi(board[y][x]);
		if (isSingleColor(crystalColor)) return -10;

		int freeDirNum = 0;
		rep(dir, DIRECTION_TYPES) {
			int nx = x + dx[dir];
			int ny = y + dy[dir];
			if (inBoard(nx, ny) && !isItemSet(nx, ny)) freeDirNum++;
		}

		int score;
		if (freeDirNum <= 1 || (crystalColor | color) == 7) score = -10;
		else score = 10;

		return score;
	}

	int calcWholeScore() {
		int score = 0;

		rep(y, H) rep(x, W) {
			if (!isCrystalSet(x, y)) continue;
			score += calcCrystalScore(x, y);
		}

		for (Item &item : solution.items) {
			if (item.type == NO_ITEM) continue;
			if (item.isLantern()) score -= costLantern;
			else if (item.isObstacle()) score -= costObtacle;
			else if (item.isMirror()) score -= costMirror;
		}

		return score;
	}

	void setLantern(int x, int y, int color) {
		board[y][x] = LANTERN;
		lanternColor[y][x] = color;

		solution.score -= costLantern;
		Point* start = new Point(x, y);
		vector<Point> cands;
		bool allMirrored = false;
		rep(dir, DIRECTION_TYPES) {
			int rdir = (dir + 2) % 4;
			Point* dest = new Point();
			int nx = x + dx[dir];
			int ny = y + dy[dir];
			int tdir = dir;
			while (isTraversable(nx, ny)) {
				destination[ny][nx][rdir] = start;
				destination[ny][nx][tdir] = dest;
				if (isMirror(nx, ny)) {
					tdir = reflectDirection(tdir, board[ny][nx]);
					rdir = reflectDirection(rdir, board[ny][nx]);
				} else {
					cands.push_back(Point(nx, ny));
				}
				nx += dx[tdir];
				ny += dy[tdir];
			}
			dest->x = nx;
			dest->y = ny;
			if (!inBoard(nx, ny) || !isCrystalSet(nx, ny)) continue;

			int tmpScore = tryLightCrystal(nx, ny, color);
			int tx = nx;
			int ty = ny;

			bool mirrored = false;
			if (tmpScore < 0 && leftMirrors) {
				int nx = x + dx[dir];
				int ny = y + dy[dir];
				int tdir = dir;
				while (isTraversable(nx, ny)) {
					if (isMirror(nx, ny)) {
						tdir = reflectDirection(tdir, board[ny][nx]);
					} else if (!isLightcrossed(nx, ny)) {
						int tmp = calcMirrorEffective(nx, ny, tdir, MIRROR_1, color, true);
						if (tmp - costMirror >= mirrorThreshold) {
							tmpScore += tmp - costMirror;
							leftMirrors--;
							mirrored = true;
							setMirror(nx, ny, MIRROR_1, tdir, color);
							break;
						}
						tmp = calcMirrorEffective(nx, ny, tdir, MIRROR_2, color, true);
						if (tmp - costMirror >= mirrorThreshold) {
							tmpScore += tmp - costMirror;
							leftMirrors--;
							mirrored = true;
							setMirror(nx, ny, MIRROR_2, tdir, color);
							break;
						}
					}
					nx += dx[tdir];
					ny += dy[tdir];
				}
			}

			if (!mirrored) lightCrystal(tx, ty, color);
			allMirrored |= mirrored;

			solution.score += tmpScore;
		}
		solution.items.push_back(Item(x, y, color));

		if (!allMirrored) trySetObstacle(cands);
	}

	void setMirror(int x, int y, char mirror, int dir, int color) {
		board[y][x] = mirror;
		calcMirrorEffective(x, y, dir, mirror, color, false);

		Point *dest[4];
		rep(dir, DIRECTION_TYPES) dest[dir] = new Point();
		rep(dir, DIRECTION_TYPES) {
			int rdir = (dir + 2) % 4;
			Point **start = &dest[(reflectDirection(dir, mirror) + 2) % 4];
			Point **goal = &dest[dir];

			int nx = x + dx[dir];
			int ny = y + dy[dir];
			int tdir = dir;
			while (isTraversable(nx, ny)) {
				destination[ny][nx][rdir] = *start;
				destination[ny][nx][tdir] = *goal;
				if (isMirror(nx, ny)) {
					tdir = reflectDirection(tdir, board[ny][nx]);
					rdir = reflectDirection(rdir, board[ny][nx]);
				}
				nx += dx[tdir];
				ny += dy[tdir];
			}

			dest[dir]->x = nx;
			dest[dir]->y = ny;
		}

		solution.items.push_back(Item(x, y, mirror));
	}

	void trySetObstacle(vector<Point> &cands) {
		for (Point &p : cands) {
			Point* dest[DIRECTION_TYPES];
			rep(dir, DIRECTION_TYPES) dest[dir] = destination[p.y][p.x][dir];

			// 光がクロスしていて、かつ両方の光がクリスタルに当たっている点のみ考慮
			int crystalCnt = 0;
			int lanternCnt = 0;
			rep(dir, DIRECTION_TYPES) {
				if (!inBoard(dest[dir]->x, dest[dir]->y)
					|| board[dest[dir]->y][dest[dir]->x] == OBSTACLE) {
					break;
				}
				crystalCnt += isCrystalSet(dest[dir]->x, dest[dir]->y);
				lanternCnt += board[dest[dir]->y][dest[dir]->x] == LANTERN;
			}
			if (crystalCnt + lanternCnt < 4) continue;

			if (calcEffectiveObstacle(dest[0], dest[2], true) <= 0) continue;
			if (calcEffectiveObstacle(dest[1], dest[3], true) <= 0) continue;

			if (leftObstacles) setObstacle(p.x, p.y);
		}
	}

	// lantern -> crystal の光を遮ってスコアが改善するか否かを調べる
	int calcEffectiveObstacle(Point* lantern, Point* crystal, bool dummy) {
		if (!inBoard(lantern->x, lantern->y) || !inBoard(crystal->x, crystal->y)) return 0;

		if (board[crystal->y][crystal->x] == LANTERN) {
			swap(lantern, crystal);
		}

		if (board[lantern->y][lantern->x] != LANTERN) return 0;
		if (!isCrystalSet(crystal->x, crystal->y)) return 0;

		int color = lanternColor[lantern->y][lantern->x];

		int prevScore = calcCrystalScore(crystal->x, crystal->y, dummy);
		lighted[crystal->y][crystal->x][color]--;
		int newScore = calcCrystalScore(crystal->x, crystal->y, dummy);
		lighted[crystal->y][crystal->x][color]++;

		return newScore - prevScore;
	}

	void setObstacle(int x, int y) {
		board[y][x] = OBSTACLE;
		leftObstacles--;

		rep(dir, 2) {
			int rdir = (dir + 2) % 4;

			Point *lantern, *crystal;
			lantern = destination[y][x][dir];
			crystal = destination[y][x][rdir];

			if (!inBoard(lantern->x, lantern->y) || !inBoard(crystal->x, crystal->y)) continue;

			if (isCrystalSet(lantern->x, lantern->y)) {
				swap(lantern, crystal);
			}
			if (board[lantern->y][lantern->x] != LANTERN || !isCrystalSet(crystal->x, crystal->y)) continue;

			int color = lanternColor[lantern->y][lantern->x];
			lighted[crystal->y][crystal->x][color]--;
		}

		rep(dir, DIRECTION_TYPES) {
			int rdir = (dir + 2) % 4;
			int nx = x + dx[dir];
			int ny = y + dy[dir];
			Point *start = new Point(x, y);
			Point *dest = new Point();
			int tdir = dir;
			while (isTraversable(nx, ny)) {
				destination[ny][nx][tdir] = dest;
				destination[ny][nx][rdir] = start;
				if (isMirror(nx, ny)) {
					tdir = reflectDirection(tdir, board[ny][nx]);
					rdir = reflectDirection(rdir, board[ny][nx]);
				}
				nx += dx[tdir];
				ny += dy[tdir];
			}
			dest->x = nx;
			dest->y = ny;
		}

		solution.items.push_back(Item(x, y, OBSTACLE));
	}

	// 1 つでも Obstacle を置いたら true を返す
	bool spendObstacles() {
		bool setFlag = false;

		rep(i, leftObstacles) {
			int maxX, maxY;
			int maxScoreDiff = 0;
			rep(y, H) rep(x, W) {
				if (board[y][x] != EMPTY) continue;
				if (leftObstacles == 0) break;
				int scoreDiff = 0;
				scoreDiff += calcEffectiveObstacle(destination[y][x][0], destination[y][x][2], false);
				scoreDiff += calcEffectiveObstacle(destination[y][x][1], destination[y][x][3], false);
				if (scoreDiff > maxScoreDiff) {
					maxScoreDiff = scoreDiff;
					maxX = x;
					maxY = y;
				}
			}
			if (maxScoreDiff < costObtacle) break;
			setObstacle(maxX, maxY);
			setFlag = true;
		}

		return setFlag;
	}

	void setDestination(int x, int y, int direction) {
		int dir = direction;
		int rdir = (direction + 2) % 4;
		Point *dest = new Point();
		Point *rdest = new Point();
		int nx = x;
		int ny = y;
		while (isTraversable(nx, ny)) {
			destination[ny][nx][dir] = dest;
			destination[ny][nx][rdir] = rdest;
			if (isMirror(nx, ny)) {
				dir = reflectDirection(dir, board[ny][nx]);
				rdir = reflectDirection(rdir, board[ny][nx]);
			}
			nx += dx[dir];
			ny += dy[dir];
		}
		dest->x = nx;
		dest->y = ny;

		dir = direction;
		rdir = (direction + 2) % 4;
		nx = x;
		ny = y;
		while (isTraversable(nx, ny)) {
			destination[ny][nx][dir] = dest;
			destination[ny][nx][rdir] = rdest;
			if (isMirror(nx, ny)) {
				dir = reflectDirection(dir, board[ny][nx]);
				rdir = reflectDirection(rdir, board[ny][nx]);
			}
			nx += dx[rdir];
			ny += dy[rdir];
		}
		rdest->x = nx;
		rdest->y = ny;
	}

	int calcMirrorEffective(int x, int y, int dir, char mirror, int color, bool test) {
		int lanternCnt = 0;
		rep(tdir, DIRECTION_TYPES) {
			Point *p = destination[y][x][tdir];
			if (!inBoard(p->x, p->y)) continue;
			if (board[p->y][p->x] == LANTERN) lanternCnt++;
		}
		if (lanternCnt >= 2) return 0;

		int rdir = reflectDirection(dir, mirror);
		Point *dest = destination[y][x][rdir];
		if (!inBoard(dest->x, dest->y)) return 0;
		if (!isCrystalSet(dest->x, dest->y)) return 0;

		return lightCrystal(dest->x, dest->y, color, test);
	}

	bool isLightcrossed(int x, int y) {
		int cnt = 0;
		rep(dir, DIRECTION_TYPES) {
			Point *p = destination[y][x][dir];
			if (inBoard(p->x, p->y) && board[p->y][p->x] == LANTERN) cnt++;
		}
		return cnt >= 2;
	}

public:
	GreedySolver(vector<string> targetBoard, int costLantern, int costMirror, int costObstacle, int maxMirrors, int maxObstacles)
		: Solver(targetBoard, costLantern, costMirror, costObstacle, maxMirrors, maxObstacles) {
		memset(lighted, 0, sizeof lighted);
		rep(y, H) rep(x, W) {
			lanternColor[y][x] = NO_LANTERN;
			rep(dir, DIRECTION_TYPES) {
				destination[y][x][dir] = NOWHERE;
			}
		}
		rep(y, H) rep(x, W) {
			rep(dir, 2) {
				if (destination[y][x][dir] != NOWHERE) continue;
				setDestination(x, y, dir);
			}
		}
	}

	Solution solve() {
		while (true) {
			if (getTime() > timeLimit) break;

			int maxX, maxY, maxColor;
			int maxScore = -1;
			int sameCnt = 1;

			rep(y, H) rep(x, W) {
				if (board[y][x] != EMPTY) continue;
				bool lightPassed = false;
				rep(dir, DIRECTION_TYPES) {
					Point *p = destination[y][x][dir];
					if (!inBoard(p->x, p->y)) continue;
					if (board[p->y][p->x] == LANTERN) {
						lightPassed = true;
						break;
					}
				}
				if (lightPassed) continue;

				int score = 0;

				int tmpLeftMirrors = leftMirrors;
				rep(color, COLOR_TYPES) {
					score = -costLantern;
					rep(dir, DIRECTION_TYPES) {
						int nx = x + dx[dir];
						int ny = y + dy[dir];
						int tdir = dir;
						while (isTraversable(nx, ny)) {
							if (isMirror(nx, ny)) {
								tdir = reflectDirection(tdir, board[ny][nx]);
							}
							nx += dx[tdir];
							ny += dy[tdir];
						}
						if (!inBoard(nx, ny)) continue;
						if (board[ny][nx] == LANTERN) {
							score = -INF;
							break;
						}
						if (!isCrystalSet(nx, ny)) continue;

						int tmpScore = tryLightCrystal(nx, ny, color);

						if (tmpScore < 0 && tmpLeftMirrors) {
							int tdir = dir;
							int nx = x + dx[tdir];
							int ny = y + dy[tdir];
							while (isTraversable(nx, ny)) {
								if (isMirror(nx, ny)) {
									tdir = reflectDirection(tdir, board[ny][nx]);
								} else if (!isLightcrossed(nx, ny)) {
									int tmp = calcMirrorEffective(nx, ny, tdir, MIRROR_1, color, true);
									if (tmp - costMirror >= mirrorThreshold) {
										tmpScore += tmp - costMirror;
										tmpLeftMirrors--;
										break;
									}
									tmp = calcMirrorEffective(nx, ny, tdir, MIRROR_2, color, true);
									if (tmp - costMirror >= mirrorThreshold) {
										tmpScore += tmp - costMirror;
										tmpLeftMirrors--;
										break;
									}
								}
								nx += dx[tdir];
								ny += dy[tdir];
							}
						}

						score += tmpScore;
					}
					if (score == maxScore && randXor() % (sameCnt + 1) == 0) {
						maxScore = score;
						maxX = x;
						maxY = y;
						maxColor = color;
						sameCnt++;
					} else if (score > maxScore) {
						maxScore = score;
						maxX = x;
						maxY = y;
						maxColor = color;
						sameCnt = 1;
					}
				}
			}

			if (maxScore <= 0) {
				if (spendObstacles() == 0) break;
			}
			else setLantern(maxX, maxY, maxColor);
		}

		for (Item &item : solution.items) {
			if (item.type > 2) continue;
			int scoreDiff = 0;
			vector<Point> crystals;
			rep(dir, DIRECTION_TYPES) {
				int nx = item.x + dx[dir];
				int ny = item.y + dy[dir];
				int tdir = dir;
				while (isTraversable(nx, ny)) {
					if (isMirror(nx, ny)) {
						tdir = reflectDirection(tdir, board[ny][nx]);
					}
					nx += dx[tdir];
					ny += dy[tdir];
				}
				if (!inBoard(nx, ny) || !isCrystalSet(nx, ny)) continue;
				int tmp = unlightCrystal(nx, ny, item.type, true);
				scoreDiff += tmp;
				crystals.push_back(Point(nx, ny));
			}
			if (scoreDiff > 0) {
				for (Point &crystal : crystals) {
					unlightCrystal(crystal.x, crystal.y, item.type, false);
				}
				item.type = NO_ITEM;
			}
		}

		// solution に格納して返すスコアはダミー値でなく真の値にしておく
		solution.score = calcWholeScore();

		return solution;
	}
};


class CrystalLighting {
public:
    vector<string> placeItems(vector<string> targetBoard, int costLantern, int costMirror, int costObstacle, int maxMirrors, int maxObstacles) {
		beginCycle = getCycle();

		Solution solution;
		solution.score = -1;
		while (getTime() < timeLimit) {
			Solver* solver = new GreedySolver(targetBoard, costLantern, costMirror, costObstacle, maxMirrors, maxObstacles);
			solution = max(solution, solver->solve());
		}

		return solution.format();
    }
};

// -------8<------- end of solution submitted to the website -------8<-------

template<class T> void getVector(vector<T>& v) {
    for (int i = 0; i < v.size(); ++i)
        cin >> v[i];
}

int main() {
    CrystalLighting cl;
    int H;
    cin >> H;
    vector<string> targetBoard(H);
    getVector(targetBoard);
    int costLantern, costMirror, costObstacle, maxMirrors, maxObstacles;
    cin >> costLantern >> costMirror >> costObstacle >> maxMirrors >> maxObstacles;

    vector<string> ret = cl.placeItems(targetBoard, costLantern, costMirror, costObstacle, maxMirrors, maxObstacles);
    cout << ret.size() << endl;
    for (int i = 0; i < (int)ret.size(); ++i)
        cout << ret[i] << endl;
    cout.flush();

	return 0;
}
